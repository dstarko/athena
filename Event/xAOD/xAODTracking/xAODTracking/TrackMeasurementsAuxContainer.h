/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKMEASUREMENTSAUXCONTAINER_H
#define XAODTRACKING_TRACKMEASUREMENTSAUXCONTAINER_H

#include "xAODTracking/versions/TrackMeasurementsAuxContainer_v1.h"

namespace xAOD {
  typedef TrackMeasurementsAuxContainer_v1 TrackMeasurementsAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackMeasurementsAuxContainer , 1120446062 , 1 )
#endif